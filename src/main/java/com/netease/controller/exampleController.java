package com.netease.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;

/**
 * Created by cuckootan on 17-3-13.
 */
@Controller
public class exampleController
{
    // path 和 value 的作用一样
    // path 可以指定一个具体的值（可包含通配符），也可以通过占位符指定一个值，还可以指定带有正则表达式的 "/spring/{userId:[a-z]+}-{tag:\\d+}" 的形式
    // path 也可以是多个 uri，比如 {"xxx", "xxx"}
    @RequestMapping(path = "/users/{userId}", params = "msg", method = RequestMethod.GET)
    public void spring(
        @PathVariable("userId") String userId,
        @RequestParam("msg") String msg,
        @RequestHeader("host") String host,
        Writer writer
    ) throws IOException
    {
        writer.write("userId = " + userId + ", msg = " + msg + ", hostHeader = " + host);
    }

    @RequestMapping(path = "/users/login", method = RequestMethod.POST)
    // 指明返回的是一个 response body，而非 view 的名字
    @ResponseBody
    public String login(
        //@RequestParam("username") String username,
        //@RequestParam("password") String password
        // 也可以直接初始化为一个实例（其中每个实例属性的名字与 request 对应的参数名相同）
        @ModelAttribute User user
    ) throws IOException
    {
        //return "Username: " + username + "<br />Password: " + password;
        return "Username: " + user.getUsername() + "<br />Password: " + user.getPassword();
    }

    @RequestMapping(path = "/users/upload", method = RequestMethod.POST)
    public void handleUpload(
        @RequestParam("file") MultipartFile file,
        HttpServletResponse resp
    ) throws IOException
    {
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html");
        resp.getWriter().write("上传成功！");

        System.out.println(file.toString());
    }

    // message converter，可以将一个 java 转变成一个 responseBody（比如 json），也可以将一个 resquestBody（比如 json）转变成一个 java 对象。
    @RequestMapping(path = "/users/transport", method = RequestMethod.GET)
    @ResponseBody
    public User transportUser()
    {
        User user = new User();
        user.setUsername("nju");
        user.setPassword("888");

        return user;
    }

    @RequestMapping(path = "/users/login2", method = RequestMethod.POST)
    // 这里返回的是一个 view 的名字。
    // 该函数调用完之后，会自动将 model map 中的数据返回到模板对应的名字中。
    public String returnView(
        @RequestParam("username") String username,
        @RequestParam("password") String password,
        ModelMap map
    ) throws IOException
    {
        // 第一个参数是 view 模板中的名字，第二个是该参数的取值
        // map.addAttribute("username", username);
        // map.addAttribute("password", password);

        // 将一个 key 以及对应的一个实例添加到 map 中。用这种方法的话，在数据模型比较复杂的时候也只需要建立一个 map 即可
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        map.addAttribute("user", user);

        System.out.println(user.getUsername());

        // view resolver 会根据返回的 view 名字自动定位到 FreeMarker 中的 user.ftl 文件
        return "user";
    }
}
