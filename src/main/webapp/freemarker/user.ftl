<html>
<head>
    <#-- 指定 html 文件的元数据。其中 charset 将告诉浏览器用 utf-8 编码来对此文件进行解码读取 -->
    <meta charset="UTF-8" />
    <meta http-equiv="content-type" content="text/html" />
    <title>用户信息</title>
</head>

<body>
    <!-- $ {xxx} 用于返回模型数据 -->
    <#--<p>Name: ${username}</p>-->
    <#--<p>Password: ${password}</p>-->

    <p>Name: ${user.username}</p>
    <p>Password: ${user.password}</p>
</body>

</html>